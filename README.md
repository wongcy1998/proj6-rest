# ACP controle times

Current version editor: Toby Wong, chunyauw @ uoregon.edu

## Description

This programme and web-based interface is based upton the ACP controle times.

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders).

An example of a model based upon, would be the following calculator (https://rusa.org/octime_acp.html).

Differences between this and proj5-mongo, is we further implement methods or api_s to extract information out of mongodb.

## How to use

Change your directory into ./DockerRestAPI

Either execute $docker-compose up , or $./run.sh as a provided shell script file.

Use your browser for the below functionalities

## Functionality

This project has following four parts. Change the values for host and port according to your machine, and use the web browser to check the results.

* "http://localhost:5002/" should be the homepage for the user interface of ACP controle times calculator

* "http://localhost:5000/" should be a homepage of an example of implementing the below-mentioned functionalities.

* "http://localhost:5001/listAll" should return all open and close times in the database
* "http://localhost:5001/listOpenOnly" should return open times only
* "http://localhost:5001/listCloseOnly" should return close times only
* "http://localhost:5001/listAll/csv" should return all open and close times in CSV format
* "http://localhost:5001/listOpenOnly/csv" should return open times only in CSV format
* "http://localhost:5001/listCloseOnly/csv" should return close times only in CSV format
* "http://localhost:5001/listAll/json" should return all open and close times in JSON format
* "http://localhost:5001/listOpenOnly/json" should return open times only in JSON format
* "http://localhost:5001/listCloseOnly/json" should return close times only in JSON format
* "http://localhost:5001/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
* "http://localhost:5001/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
* "http://localhost:5001/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
* "http://localhost:5001/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format
