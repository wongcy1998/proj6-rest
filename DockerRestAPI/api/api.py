from flask import Flask, request
from flask_restful import Resource, Api
from pymongo import MongoClient

app = Flask(__name__)
api = Api(app)
client = MongoClient("db", 27017)
db = client.tododb

class _listAll(Resource):
    def get(self):
        top = request.args.get("top")
        if top == None:
            top = 1024
        db_finder = db.tododb.find().sort("_open_time", 1).limit(int(top))
        full_db = [i for i in db_finder]
        return {
            '_open_time': [i['_open'] for i in full_db],
            '_close_time': [i['_close'] for i in full_db]
        }

class _listAll_JSON(Resource):
    def get(self):
        top = request.args.get("top")
        if top == None:
            top = 1024
        db_finder = db.tododb.find().sort("_open_time", 1).limit(int(top))
        full_db = [i for i in db_finder]
        return {
            '_open_time': [i['_open'] for i in full_db],
            '_close_time': [i['_close'] for i in full_db]
        }

class _listAll_CSV(Resource):
    def get(self):
        top = request.args.get("top")
        if top == None:
            top = 1024
        db_finder = db.tododb.find().sort("_open_time", 1).limit(int(top))
        full_db = [i for i in db_finder]
        csv = ""
        for i in full_db:
            csv += i['_open'] + ', ' + i['_close'] + ', '
        return csv

class _listOpen(Resource):
    def get(self):
        top = request.args.get("top")
        if top == None:
            top = 1024
        db_finder = db.tododb.find().sort("_open_time", 1).limit(int(top))
        return {
            '_open_time': [i['_open'] for i in db_finder]
        }

class _listOpen_JSON(Resource):
    def get(self):
        top = request.args.get("top")
        if top == None:
            top = 1024
        db_finder = db.tododb.find().sort("_open_time", 1).limit(int(top))
        return {
            '_open_time': [i['_open'] for i in db_finder]
        }

class _listOpen_CSV(Resource):
    def get(self):
        top = request.args.get("top")
        if top == None:
            top = 1024
        db_finder = db.tododb.find().sort("_open_time", 1).limit(int(top))
        full_db = [i for i in db_finder]
        csv = ""
        for i in full_db:
            csv += i['_open'] + ', '
        return csv

class _listClose(Resource):
    def get(self):
        top = request.args.get("top")
        if top == None:
            top = 1024
        db_finder = db.tododb.find().sort("_close_time", 1).limit(int(top))
        return {
            '_close_time': [i['_close'] for i in db_finder]
        }

class _listClose_JSON(Resource):
    def get(self):
        top = request.args.get("top")
        if top == None:
            top = 1024
        db_finder = db.tododb.find().sort("_close_time", 1).limit(int(top))
        return {
            '_close_time': [i['_close'] for i in db_finder]
        }

class _listClose_CSV(Resource):
    def get(self):
        top = request.args.get("top")
        if top == None:
            top = 1024
        db_finder = db.tododb.find().sort("_close_time", 1).limit(int(top))
        full_db = [i for i in db_finder]
        csv = ""
        for i in full_db:
            csv += i['_close'] + ', '
        return csv

api.add_resource(_listAll, '/listAll')
api.add_resource(_listAll_JSON, '/listAll/json')
api.add_resource(_listAll_CSV, '/listAll/csv')
api.add_resource(_listOpen, '/listOpenOnly')
api.add_resource(_listOpen_JSON, '/listOpenOnly/json')
api.add_resource(_listOpen_CSV, '/listOpenOnly/csv')
api.add_resource(_listClose, '/listCloseOnly')
api.add_resource(_listClose_JSON, '/listCloseOnly/json')
api.add_resource(_listClose_CSV, '/listCloseOnly/csv')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
