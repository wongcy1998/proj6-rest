"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

checkpoints = [200, 400, 600, 1000, 1300]
max_speed = [34, 32, 30, 28, 26]
min_speed = [15, 15, 15, 11.428, 13.333]

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    op_ACP = arrow.get(brevet_start_time)
    hour = 0
    
    ### If the control distance is larger than checkpoint distance, replace
    if control_dist_km > brevet_dist_km:
       control_dist_km = brevet_dist_km
    remainder_control_dist_km = control_dist_km
   
    ### Loop through all the checkpoints
    for i in range(5):
       ### Run if our control distance is within the checkpoint of loop
       ### If origin
       if i == 0:
          distance_between = min(remainder_control_dist_km, checkpoints[i])
       ### If not origin
       else:
          ### Calculate the distance between current and previous checkpoint.
          distance_between = min(remainder_control_dist_km, (checkpoints[i]-checkpoints[i-1]))
       ### Append the time required to travel through the checkpoints.
       hour += round(distance_between/max_speed[i],2)
       remainder_control_dist_km -= distance_between
    if control_dist_km < 0:
       hour = 0
    return op_ACP.shift(hours = hour).isoformat()



def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    op_ACP = arrow.get(brevet_start_time)
    hour = 0
    
    ### If the control distance is larger than checkpoint distance, replace
    if control_dist_km > brevet_dist_km:
       control_dist_km = brevet_dist_km
    remainder_control_dist_km = control_dist_km
   
    ### Loop through all the checkpoints
    for i in range(5):
       ### Run if our control distance is within the checkpoint of loop
       ### If origin
       if i == 0:
          distance_between = min(remainder_control_dist_km, checkpoints[i])
       ### If not origin
       else:
          ### Calculate the distance between current and previous checkpoint.
          distance_between = min(remainder_control_dist_km, (checkpoints[i]-checkpoints[i-1]))
       ### Append the time required to travel through the checkpoints.
       hour += round(distance_between/min_speed[i],2)
       remainder_control_dist_km -= distance_between
    if hour == 0:
       hour = 1
    return op_ACP.shift(hours = hour).isoformat()